PROJECT_NAME = shop-fence

SRC_DIR = src
BUILD_DIR = $(PROJECT_NAME)
ARCHIVE_DIR = dist
LANGUAGES_DIR = languages

TEXT_DOMAIN = $(PROJECT_NAME)
POT_FILE = $(BUILD_DIR)/$(LANGUAGES_DIR)/$(TEXT_DOMAIN).pot
LOCALE ?= $(LANG)
PO_FILE = $(BUILD_DIR)/$(LANGUAGES_DIR)/$(TEXT_DOMAIN)-$(LOCALE).po
MO_FILE = $(BUILD_DIR)/$(LANGUAGES_DIR)/$(TEXT_DOMAIN)-$(LOCALE).mo

all:

me-started: $(BUILD_DIR)/vendor

archive: me-started
	composer archive --format=zip --dir=$(ARCHIVE_DIR)
	rsync -av $(ARCHIVE_DIR)/. kevinparka@raoula.caboulot.org:public_html/

.PHONY: me-started

$(BUILD_DIR)/vendor: composer.lock
	composer install

composer.lock: composer.json
	composer update

tmp/trunk/tools/i18n/makepot.php:
	[ "which svn" != "" ] && exit "Should sudo apt-get install subversion ..."
	mkdir tmp; cd tmp; svn co https://develop.svn.wordpress.org/trunk/

$(POT_FILE): tmp/trunk/tools/i18n/makepot.php
	php tmp/trunk/tools/i18n/makepot.php wp-plugin $(BUILD_DIR)
	mv $(TEXT_DOMAIN).pot $@

create_pot_file: $(POT_FILE)

remove_pot_file: $(POT_FILE)
	rm -f $(POT_FILE)

update_pot_file:
	make remove_pot_file
	make create_pot_file

create_po_file: update_pot_file
	msginit --input=$(POT_FILE) --locale=$(LOCALE) --no-translator --output-file=$(PO_FILE)

update_po_file: update_pot_file
	msgmerge --update $(PO_FILE) $(POT_FILE)

generate_mo_file: $(PO_FILE)
	msgfmt --output-file=$(MO_FILE) $(PO_FILE)

update_po_files:
	make update_po_file LANG=en_US
	make update_po_file LANG=fr_FR

generate_mo_files:
	make generate_mo_file LANG=en_US
	make generate_mo_file LANG=fr_FR
