# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.1-dev] - 2016-11-17
- Fully localized sources. Available translations in fr_FR and en_US.

## [0.1.0-dev] - 2016-11-14
- Initial release