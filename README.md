# Archives

* Latest : [kevinparka-shop-fence-0.1.1-dev.zip](http://raoula.caboulot.org/~kevinparka/kevinparka-shop-fence-0.1.1-dev.zip)
* [kevinparka-shop-fence-0.1.0-dev.zip](http://raoula.caboulot.org/~kevinparka/kevinparka-shop-fence-0.1.0-dev.zip)

# Useful resources

* https://developer.wordpress.org/plugins/ Wordpress plugin developper handbook
* https://www.gnu.org/software/gettext/manual/gettext.html GNU Gettext manual
* http://keepachangelog.com/en/0.3.0/ Changelog format

# Developers

## Requirements

* Debian packages : subversion, gettext

## Makefile

### Build

`make archive` will create a ready-to-install zip file in the `dist` folder with
the current version number (see `composer.json`).
The archives wiil be rsynced to caboulo.org automatically if authorized.

### Install

Unzip the archive (see Build above) in Wordpress plugin directory (which must
probably be `wp-content\ plugins`).

Then activate the plugin in Wordpress backoffice.

### I18n

`make create_pot_file` and `make update_pot_file` in order to, respectively
generate a brand new pot file from sources files and update a previously
generated pot file from changed sources files.

`make create_po_file LANG=fr_FR` and `make update_po_file LANG=fr_FR` in order
to, respectively, generate a new pot file for locale fr_FR (for exemple)
relatively to the existing pot file and update a previously generated po file
from changed pot file.

