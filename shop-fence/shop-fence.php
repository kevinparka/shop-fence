<?php

/**
 * Plugin Name: Shop Fence
 * Plugin URI: https://framagit.org/kevinparka/shop-fence
 * Version: 0.1.1-dev
 * Author: Kevin Parka
 * Description: Allow, password protect or forbid access to WooCommerce's frontend.
 * Text Domain: shop-fence
 * Domain Path: /languages
 * License: CC0
 * Licence URI: http://creativecommons.org/publicdomain/zero/1.0/
 *
 */

defined('WPINC') or die();

require __DIR__.'/vendor/autoload.php';

load_plugin_textdomain('shop-fence', false, basename(dirname(__FILE__)).'/languages/');

// Configuration

define('SHOP_FENCE_PREFIX', 'sf_');

// Enter/exit forms

define('SHOP_FENCE_FIELD_ACTION', 'action');
define('SHOP_FENCE_ACTION_ENTER', SHOP_FENCE_PREFIX.'enter');
define('SHOP_FENCE_ACTION_EXIT', SHOP_FENCE_PREFIX.'exit');
define('SHOP_FENCE_FIELD_PASSWORD', 'password');

// Storage keys

define('SHOP_FENCE_KEY_MESSAGES', SHOP_FENCE_PREFIX.'messages');
define('SHOP_FENCE_KEY_URL', SHOP_FENCE_PREFIX.'url');

// Messages

define('SHOP_FENCE_MESSAGE_INVALID_TOKEN', __('message.invalid.token', 'shop-fence'));
define('SHOP_FENCE_MESSAGE_INVALID_PASSWORD', __('message.invalid.password', 'shop-fence'));

// Shortcode

define('SHOP_FENCE_SHORTCODE_NAME', 'shopfence');

// Statuses

define('SHOP_FENCE_STATUS_OPEN', 'open');
define('SHOP_FENCE_STATUS_PROTECTED', 'protected');
define('SHOP_FENCE_STATUS_CLOSED', 'closed');
define('SHOP_FENCE_DEFAULT_STATUS', SHOP_FENCE_STATUS_PROTECTED);

// Settings

define('SHOP_FENCE_SETTINGS_PAGE_SLUG', 'reading');
define('SHOP_FENCE_SETTINGS_SECTION_SLUG', SHOP_FENCE_PREFIX.'settings');
define('SHOP_FENCE_SETTINGS_SECTION_TITLE', __('settings.section.title', 'shop-fence'));
define('SHOP_FENCE_SETTINGS_SECTION_DESCRIPTION', __('settings.section.description', 'shop-fence'));

define('SHOP_FENCE_SETTING_PASSWORD_SLUG', SHOP_FENCE_PREFIX.'password');
define('SHOP_FENCE_SETTING_PASSWORD_TITLE', __('setting.password.title', 'shop-fence'));
define('SHOP_FENCE_SETTING_PASSWORD_DESCRIPTION', __('setting.password.description', 'shop-fence'));
define('SHOP_FENCE_SETTING_PASSWORD_DEFAULT', '');

define('SHOP_FENCE_SETTING_PAGE_SLUG', SHOP_FENCE_PREFIX.'page');
define('SHOP_FENCE_SETTING_PAGE_TITLE', __('setting.page.title', 'shop-fence'));
define('SHOP_FENCE_SETTING_PAGE_DESCRIPTION', __('setting.page.description', 'shop-fence'));
define('SHOP_FENCE_SETTING_PAGE_DEFAULT', '');

define('SHOP_FENCE_SETTING_STATUS_SLUG', SHOP_FENCE_PREFIX.'status');
define('SHOP_FENCE_SETTING_STATUS_TITLE', __('setting.status.title', 'shop-fence'));
define('SHOP_FENCE_SETTING_STATUS_DESCRIPTION', __('setting.status.description', 'shop-fence'));
define('SHOP_FENCE_SETTING_STATUS_DEFAULT', SHOP_FENCE_DEFAULT_STATUS);

define('SHOP_FENCE_SETTING_SOURCES_SLUG', SHOP_FENCE_PREFIX.'sources');
define('SHOP_FENCE_SETTING_SOURCES_TITLE', __('setting.sources.title', 'shop-fence'));
define('SHOP_FENCE_SETTING_SOURCES_DESCRIPTION', __('setting.sources.description', 'shop-fence'));

// Hooks

add_action('wp', 'shopFenceGuard');
add_action('admin_post_'.SHOP_FENCE_ACTION_ENTER, 'shopFenceEnter');
add_action('admin_post_'.SHOP_FENCE_ACTION_EXIT, 'shopFenceExit');
add_action('admin_post_nopriv_'.SHOP_FENCE_ACTION_ENTER, 'shopFenceEnter');
add_action('admin_post_nopriv_'.SHOP_FENCE_ACTION_EXIT, 'shopFenceExit');
add_action('admin_init', 'shopFenceAdminInit');

add_filter('widget_text', 'do_shortcode');

add_shortcode(SHOP_FENCE_SHORTCODE_NAME, 'shopFenceShortcode');

// Session

if (!session_id()) {
    session_start();
}

// I18n

// Functions

function shopFenceRedirect($url, $message = null)
{
    if (wp_redirect($url)) {
        die();
    } else {
        wp_die($message);
    }
}

function shopFenceRedirectToPage($message = null)
{
    shopFenceRedirect(shopFenceGetPage(), $message);
}

function shopFenceIsValidPassword($givenPassword)
{
    return
        is_string($givenPassword) &&
        !empty($givenPassword)
    ;
}

function shopFenceVerifyPassword($givenPassword)
{
    $currentPassword = get_option(SHOP_FENCE_SETTING_PASSWORD_SLUG);

    return
        shopFenceIsValidPassword($currentPassword) &&
        shopFenceIsValidPassword($givenPassword) &&
        ($currentPassword === $givenPassword)
    ;
}

function shopFenceGetStatuses()
{
    return [
        SHOP_FENCE_STATUS_OPEN,
        SHOP_FENCE_STATUS_PROTECTED,
        SHOP_FENCE_STATUS_CLOSED,
    ];
}

function shopFenceGetStatus()
{
    return get_option(SHOP_FENCE_SETTING_STATUS_SLUG);
}


function shopFenceIsStatus($givenStatus)
{
    $statuses = shopFenceGetStatuses();
    $defaultStatus = reset($statuses);

    $currentStatus = shopFenceGetStatus();
    if (empty($currentStatus)) {
            $currentStatus = $defaultStatus;
    }

    return
        is_string($givenStatus) &&
        ($givenStatus === $currentStatus)
    ;
}

function shopFenceGetPage()
{
    $currentPage = get_option(SHOP_FENCE_SETTING_PAGE_SLUG);

    return empty($currentPage) ? home_url() : get_permalink(get_page_by_path($currentPage));
}

function shopFenceGetAllSources()
{
    /** @see https://docs.woocommerce.com/wc-apidocs/package-WooCommerce.Functions.html */
    /** @see https://docs.woocommerce.com/wc-apidocs/function-wc_get_page_id.html */

    $sourcesDefinitions = [
        ['type' => 'page', 'name' => 'myaccount'],
        ['type' => 'page', 'name' => 'edit_address'],
        ['type' => 'page', 'name' => 'shop'],
        ['type' => 'page', 'name' => 'cart'],
        ['type' => 'page', 'name' => 'checkout'],
        ['type' => 'page', 'name' => 'pay'],
        ['type' => 'page', 'name' => 'view_order'],
        ['type' => 'page', 'name' => 'terms'],
        ['type' => 'function', 'name' => 'is_shop'],
        ['type' => 'function', 'name' => 'is_account_page'],
        ['type' => 'function', 'name' => 'is_add_payment_method_page'],
        ['type' => 'function', 'name' => 'is_cart'],
        ['type' => 'function', 'name' => 'is_checkout'],
        ['type' => 'function', 'name' => 'is_checkout_pay_page'],
        ['type' => 'function', 'name' => 'is_edit_account_page'],
        ['type' => 'function', 'name' => 'is_lost_password_page'],
        ['type' => 'function', 'name' => 'is_order_received_page'],
        ['type' => 'function', 'name' => 'is_product'],
        ['type' => 'function', 'name' => 'is_product_category'],
        ['type' => 'function', 'name' => 'is_product_tag'],
        ['type' => 'function', 'name' => 'is_product_taxonomy'],
        ['type' => 'function', 'name' => 'is_store_notice_showing'],
        ['type' => 'function', 'name' => 'is_view_order_page'],
        ['type' => 'function', 'name' => 'is_wc_endpoint_url'],
        ['type' => 'function', 'name' => 'is_woocomerce'],
    ];

    $sources = [];

    foreach ($sourcesDefinitions as $sourceDefinition) {
        $sourceKey = md5(json_encode($sourceDefinition));

        $sourceLabel = $sourceKey;

        switch ($sourceDefinition['type']) {
        case 'page' :
        case 'function' :
            $sourceLabel = $sourceDefinition['type'].' '.$sourceDefinition['name'];
            break;
        }

        $sources[$sourceKey] = array_merge(['label' => $sourceLabel], $sourceDefinition);
    }

    return $sources;
}

function shopFenceIsSource($givenSource)
{
    $sources = shopFenceGetAllSources();

    return isset($sources[$givenSource]);
}

function shopFenceGetCurrentSources()
{
    return get_option(SHOP_FENCE_SETTING_SOURCES_SLUG);
}

function shopFenceIsValidSource($source)
{
    return
        is_array($source) &&
        !empty($source['type']) &&
        in_array($source['type'], ['page', 'function'])
    ;
}

function shopFenceVerifySource($source, $queriedObject, $defaultIsVerified = false)
{
    $isVerified = $defaultIsVerified;

    if (!shopFenceIsValidSource($source)) {
        return $isVerified;
    }

    switch ($source['type']) {

    case 'page' :
        if (
            empty($source['name']) ||
            !is_object($queriedObject)
        ) {
            return $isVerified;
        }

        $pageId = (int) wc_get_page_id($source['name']);

        $isVerified = is_page($queriedObject) && ((int) $queriedObject->ID === $pageId);
        break;

    case 'function' :
        if (empty($source['name'])) {
            return $isVerified;
        }

        $functionName = $source['name'];

        $isVerified = function_exists($functionName) && $functionName();
        break;

    }

    return $isVerified;
}

function shopFenceVerifySources($queriedObject, $defaultIsVerified = false)
{
    $isVerified = $defaultIsVerified;

    if (
        is_admin() ||
        !is_object($queriedObject)
    ) {
        return $isVerified;
    }

    $allSources = shopFenceGetAllSources();
    $currentSources = shopFenceGetCurrentSources();

    $isVerified = false;

    foreach ($currentSources as $sourceKey) {
        if (shopFenceIsSource($sourceKey)) {
            $source = $allSources[$sourceKey];

            $isVerified = $isVerified || shopFenceVerifySource($source, $queriedObject);
        }
    }

    return $isVerified;
}

function shopFenceVerifySessionPassword($defaultIsVerified = false)
{
    return !empty($_SESSION[SHOP_FENCE_FIELD_PASSWORD]) ? shopFenceVerifyPassword($_SESSION[SHOP_FENCE_FIELD_PASSWORD]) : $defaultIsVerified;
}

function shopFenceVerifyRequestPassword($defaultIsVerified = false)
{
    return !empty($_REQUEST[SHOP_FENCE_FIELD_PASSWORD]) ? shopFenceVerifyPassword($_REQUEST[SHOP_FENCE_FIELD_PASSWORD]) : $defaultIsVerified;
}

function shopFenceShouldRedirect($defaultShouldRedirect = false)
{
    switch (shopFenceGetStatus()) {

    default:
    case SHOP_FENCE_STATUS_OPEN :
        $shouldRedirect = $defaultShouldRedirect;
        break;

    case SHOP_FENCE_STATUS_PROTECTED :
        $shouldRedirect = !shopFenceVerifySessionPassword();
        break;

    case SHOP_FENCE_STATUS_CLOSED :
        $shouldRedirect = true;
        break;

    }

    return $shouldRedirect;
}

function shopFenceGuard() {
    global $wp;

    $currentUrl = add_query_arg($wp->query_string, '', home_url($wp->request));

    $queriedObject = get_queried_object();

    if (
        shopFenceVerifySources($queriedObject) &&
        shopFenceShouldRedirect()
    ) {
        $_SESSION[SHOP_FENCE_KEY_URL] = $currentUrl;
        shopFenceRedirectToPage();
    }
}

function shopFenceShortcode() {
    if (!empty($_SESSION[SHOP_FENCE_KEY_MESSAGES])) {
        $messages = $_SESSION[SHOP_FENCE_KEY_MESSAGES];
        unset($_SESSION[SHOP_FENCE_KEY_MESSAGES]);
    } else {
        $messages = [];
    }
    $messages[] = esc_html(shopFenceGetStatusLabel(shopFenceGetStatus()));
    ob_start(); ?>
<?php if (!empty($messages)) : ?>
<ul><li><?php echo implode('</li><li>', $messages); ?></li></ul>
<?php endif; ?>
<?php if (shopFenceIsStatus(SHOP_FENCE_STATUS_PROTECTED)) : ?>
    <?php if (shopFenceVerifySessionPassword()) : ?>
    <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" method="post">
        <button type="submit" name="<?php echo esc_attr(SHOP_FENCE_FIELD_ACTION); ?>" value="<?php echo esc_attr(SHOP_FENCE_ACTION_EXIT); ?>"><?php echo esc_html(__('shortcode.form.button.exit', 'shop-fence')); ?></button>
        <?php wp_nonce_field(SHOP_FENCE_ACTION_EXIT); ?>
    </form>
    <?php else : ?>
    <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" method="post">
        <input type="password" name="<?php echo esc_attr(SHOP_FENCE_FIELD_PASSWORD); ?>"/>
        <button type="submit" name="<?php echo esc_attr(SHOP_FENCE_FIELD_ACTION); ?>" value="<?php echo esc_attr(SHOP_FENCE_ACTION_ENTER); ?>"><?php echo esc_html(__('shortcode.form.button.enter', 'shop-fence')); ?></button>
        <?php wp_nonce_field(SHOP_FENCE_ACTION_ENTER); ?>
    </form>
    <?php endif; ?>
<?php endif; ?>
<?php return ob_get_clean();
}

function shopFenceEnter() {
    $messages = [];

    if (
        !check_admin_referer(SHOP_FENCE_ACTION_ENTER) ||
        !wp_verify_nonce($_REQUEST['_wpnonce'], SHOP_FENCE_ACTION_ENTER)
    ) {
        $messages[] = SHOP_FENCE_INVALID_TOKEN;

        wp_die(implode("\n", $messages));
    }

    if (
        empty($_REQUEST[SHOP_FENCE_FIELD_PASSWORD]) ||
        !shopFenceVerifyRequestPassword()
    ) {
        $messages[] = SHOP_FENCE_MESSAGE_INVALID_PASSWORD;

        $_SESSION[SHOP_FENCE_KEY_MESSAGES] = $messages;

        $originalUrl = wp_get_referer();
    } else {
        $_SESSION[SHOP_FENCE_FIELD_PASSWORD] = $_REQUEST[SHOP_FENCE_FIELD_PASSWORD];

        $originalUrl = (!empty($_SESSION[SHOP_FENCE_KEY_URL]) ? $_SESSION[SHOP_FENCE_KEY_URL] : wp_get_referer());
        unset($_SESSION[SHOP_FENCE_KEY_URL]);
    }

    shopFenceRedirect($originalUrl);
}

function shopFenceExit() {
    $messages = [];

    if (
        !check_admin_referer(SHOP_FENCE_ACTION_EXIT) ||
        !wp_verify_nonce($_REQUEST['_wpnonce'], SHOP_FENCE_ACTION_EXIT)
    ) {
        $messages[] = SHOP_FENCE_MESSAGE_INVALID_TOKEN;

        $_SESSION[SHOP_FENCE_KEY_MESSAGES] = $messages;

        shopFenceRedirectToPage(implode("\n", $messages));
    }

    unset($_SESSION[SHOP_FENCE_FIELD_PASSWORD]);

    $originalUrl = (!empty($_SESSION[SHOP_FENCE_KEY_URL]) ? $_SESSION[SHOP_FENCE_KEY_URL] : wp_get_referer());
    unset($_SESSION[SHOP_FENCE_KEY_URL]);

    shopFenceRedirect($originalUrl);
}

function shopFenceAdminInit()
{
    add_settings_section(
        SHOP_FENCE_SETTINGS_SECTION_SLUG,
        SHOP_FENCE_SETTINGS_SECTION_TITLE,
        'shopFenceSettingsSection',
        SHOP_FENCE_SETTINGS_PAGE_SLUG
    );

    // Sources

    add_option(SHOP_FENCE_SETTING_SOURCES_SLUG, [], '', true);

    add_settings_field(
        SHOP_FENCE_SETTING_SOURCES_SLUG,
        SHOP_FENCE_SETTING_SOURCES_TITLE,
        'shopFenceSettingSources',
        SHOP_FENCE_SETTINGS_PAGE_SLUG,
        SHOP_FENCE_SETTINGS_SECTION_SLUG
    );

    register_setting(SHOP_FENCE_SETTINGS_PAGE_SLUG, SHOP_FENCE_SETTING_SOURCES_SLUG);

    // Status

    add_option(SHOP_FENCE_SETTING_STATUS_SLUG, SHOP_FENCE_SETTING_STATUS_DEFAULT, '', true);

    add_settings_field(
        SHOP_FENCE_SETTING_STATUS_SLUG,
        SHOP_FENCE_SETTING_STATUS_TITLE,
        'shopFenceSettingStatus',
        SHOP_FENCE_SETTINGS_PAGE_SLUG,
        SHOP_FENCE_SETTINGS_SECTION_SLUG
    );

    register_setting(SHOP_FENCE_SETTINGS_PAGE_SLUG, SHOP_FENCE_SETTING_STATUS_SLUG);

    // Password

    add_option(SHOP_FENCE_SETTING_PASSWORD_SLUG, SHOP_FENCE_SETTING_PASSWORD_DEFAULT, '', true);

    add_settings_field(
        SHOP_FENCE_SETTING_PASSWORD_SLUG,
        SHOP_FENCE_SETTING_PASSWORD_TITLE,
        'shopFenceSettingPassword',
        SHOP_FENCE_SETTINGS_PAGE_SLUG,
        SHOP_FENCE_SETTINGS_SECTION_SLUG
    );

    register_setting(SHOP_FENCE_SETTINGS_PAGE_SLUG, SHOP_FENCE_SETTING_PASSWORD_SLUG);

    // Page

    add_option(SHOP_FENCE_SETTING_PAGE_SLUG, SHOP_FENCE_SETTING_PAGE_DEFAULT, '', true);

    add_settings_field(
        SHOP_FENCE_SETTING_PAGE_SLUG,
        SHOP_FENCE_SETTING_PAGE_TITLE,
        'shopFenceSettingPage',
        SHOP_FENCE_SETTINGS_PAGE_SLUG,
        SHOP_FENCE_SETTINGS_SECTION_SLUG
    );

    register_setting(SHOP_FENCE_SETTINGS_PAGE_SLUG, SHOP_FENCE_SETTING_PAGE_SLUG);
}

function shopFenceSettingsSection()
{
    ob_start();
?>
<p><?php echo esc_html(SHOP_FENCE_SETTINGS_SECTION_DESCRIPTION); ?></p>
<?php
    echo ob_get_clean();
}

function shopFenceSettingPassword()
{
    ob_start();
?>
<input id="<?php echo esc_attr(SHOP_FENCE_SETTING_PASSWORD_SLUG); ?>" type="password" name="<?php echo esc_attr(SHOP_FENCE_SETTING_PASSWORD_SLUG); ?>" value="<?php echo esc_attr(get_option(SHOP_FENCE_SETTING_PASSWORD_SLUG)); ?>"/>
<p class="description"><?php echo esc_attr(SHOP_FENCE_SETTING_PASSWORD_DESCRIPTION); ?></p>
<?php
    echo ob_get_clean();
}

function shopFenceSettingPage()
{
    $allPages = get_posts([
        'post_type' => 'page',
        'posts_per_page' => -1,
        'orderby' => 'title',
    ]);

    $currentPage = get_option(SHOP_FENCE_SETTING_PAGE_SLUG);

    ob_start();
?>
<select id="<?php echo esc_attr(SHOP_FENCE_SETTING_PAGE_SLUG); ?>" name="<?php echo esc_attr(SHOP_FENCE_SETTING_PAGE_SLUG); ?>">
<option value=""></option>
<?php foreach ($allPages as $page) : ?>
<option value="<?php echo esc_attr($page->post_name); ?>"<?php if ($page->post_name === $currentPage) : ?> selected<?php endif; ?>><?php echo esc_html($page->post_title); ?></option>
<?php endforeach; ?>
</select>
<p class="description"><?php echo esc_attr(SHOP_FENCE_SETTING_PAGE_DESCRIPTION); ?></p>
<?php
    echo ob_get_clean();
}

function shopFenceGetStatusLabel($status)
{
    $statusesLabels = [
        SHOP_FENCE_STATUS_OPEN => __('status.label.open', 'shop-fence'),
        SHOP_FENCE_STATUS_PROTECTED => __('status.label.protected', 'shop-fence'),
        SHOP_FENCE_STATUS_CLOSED => __('status.label.closed', 'shop-fence'),
    ];

    return isset($statusesLabels[$status]) ? $statusesLabels[$status] : $status;
}

function shopFenceSettingStatus()
{
    $statuses = shopFenceGetStatuses();

    ob_start();
?>
<?php foreach ($statuses as $status) : ?>
<p>
    <label>
        <input id="<?php echo esc_attr(SHOP_FENCE_SETTING_STATUS_SLUG); ?>" type="radio" name="<?php echo esc_attr(SHOP_FENCE_SETTING_STATUS_SLUG); ?>" value="<?php echo esc_attr($status); ?>"<?php if (shopFenceIsStatus($status)) : ?> checked="checked"<?php endif ?>/>
        <?php echo esc_html(shopFenceGetStatusLabel($status)); ?>
    </label>
</p>
<?php endforeach; ?>
<p class="description"><?php echo esc_attr(SHOP_FENCE_SETTING_STATUS_DESCRIPTION); ?></p>
<?php
    echo ob_get_clean();
}

function shopFenceSettingSources()
{
    $allSources = shopFenceGetAllSources();
    $currentSources = shopFenceGetCurrentSources();
    if (!is_array($currentSources)) {
        $currentSources = [];
    }

    ob_start();
?>
<?php $index = 0; foreach ($allSources as $sourceKey => $source) : ?>
<p>
    <label>
        <input id="<?php echo esc_attr(SHOP_FENCE_SETTING_SOURCES_SLUG.$index); ?>" type="checkbox" name="<?php echo esc_attr(SHOP_FENCE_SETTING_SOURCES_SLUG.'['.$index.']'); ?>" value="<?php echo esc_attr($sourceKey); ?>"<?php if (in_array($sourceKey, $currentSources)) : ?> checked="checked"<?php endif ?>/>
        <?php echo esc_html(!empty($source['label']) ? $source['label'] : $sourceKey); ?>
    </label>
</p>
<?php $index += 1; endforeach; ?>
<p class="description"><?php echo esc_attr(SHOP_FENCE_SETTING_SOURCES_DESCRIPTION); ?></p>
<?php
    echo ob_get_clean();
}
