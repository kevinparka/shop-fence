��          �      |      �  C   �     5  
   A  *   L     w     �     �     �     �     �          "     8     S     h     �     �     �     �     �     �  �    C   �     �  
   �  *        1     B  +   P     |  !   �     �     �     �     �     �     �     �                    %     *                                              	                                   
                Allow, password protect or forbid access to WooCommerce's frontend. Kevin Parka Shop Fence https://framagit.org/kevinparka/shop-fence message.invalid.password message.invalid.token setting.page.description setting.page.title setting.password.description setting.password.title setting.sources.description setting.sources.title setting.status.description setting.status.title settings.section.description settings.section.title shortcode.form.button.enter shortcode.form.button.exit status.label.closed status.label.open status.label.protected Project-Id-Version: Shop Fence 0.1.0-dev
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/shop-fence
POT-Creation-Date: 2016-11-14 18:22:05+00:00
PO-Revision-Date: 2016-11-13 14:09:55+00:00
Last-Translator: Automatically generated
Language-Team: none
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Allow, password protect or forbid access to WooCommerce's frontend. Kevin Parka Shop Fence https://framagit.org/kevinparka/shop-fence Invalid password Invalid token Where to redirect to? (Wordpress page slug) Page Password required when protected. Password Conditions watch list. Sources Current status. Status Here can be setup Shop Fence Shop Fence settings Enter Exit Closed Open Password protected 