��          �      |      �  C   �     5  
   A  *   L     w     �     �     �     �     �          "     8     S     h     �     �     �     �     �     �  �    b   �     
       *   )     T     j  @   y     �  I   �     	  ;     
   R     ]     x  4   ~  $   �     �     �     �     �     �                                              	                                   
                Allow, password protect or forbid access to WooCommerce's frontend. Kevin Parka Shop Fence https://framagit.org/kevinparka/shop-fence message.invalid.password message.invalid.token setting.page.description setting.page.title setting.password.description setting.password.title setting.sources.description setting.sources.title setting.status.description setting.status.title settings.section.description settings.section.title shortcode.form.button.enter shortcode.form.button.exit status.label.closed status.label.open status.label.protected Project-Id-Version: Shop Fence 0.1.0-dev
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/shop-fence
POT-Creation-Date: 2016-11-14 18:22:05+00:00
PO-Revision-Date: 2016-11-13 13:55:42+00:00
Last-Translator: Automatically generated
Language-Team: none
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Permet d'ouvrir, de protéger par mot de passe ou d'interdire l'accès à la boutique WooCommerce. Kevin Parka Grille de boutique https://framagit.org/kevinparka/shop-fence Mot de passe invalide Jeton invalide Slug de page Wordpress vers laquelle rediriger le cas échéant. Page Mot de passe à entrer quand la boutique est protégée par mot de passe. Mot de passe Conditions à vérifier pour être confronté à la grille. Conditions État courant de la grille État Ici peut-être paramétré la grille de la boutique. Paramètres de la grille de boutique Entrer Sortir Fermé Ouvert Protégé par mot de passe 